#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

pid_t padre_pid;
pid_t hijos_pids[100];
int aux, cont = 0;
int h1 = 0, h2 = 0, t = 0;

void hijos(int sig);
void hijo2(int sig);
void TERM(int sig);

int main() {
    padre_pid = getpid();
    signal(SIGUSR1, hijos);
    signal(SIGUSR2, hijo2);
    signal(SIGTERM, TERM);

    printf("padre: PID=%d\n", getpid());

    h1 = 0;
    h2 = 0;
    t = 0;

    while (1) {
        if (t == 1) {
            printf("Recibida la señal SIGTERM. Terminando todos los hijos...\n");
            for (int i = 0; i < cont; i++) {
                printf("Hijo PID:%d\n", hijos_pids[i]);
                kill(hijos_pids[i], SIGTERM);
                sleep(1);
            }
            t = 0;
        }

        if (h1 != 0) {
            pid_t hijo_pid = fork();
            aux = hijo_pid;
            if (hijo_pid == 0) {
                printf("Soy el hijo1 con PID: %d, PID del padre: %d\n", getpid(), padre_pid);
                sleep(5);
pause();
            }
        }
pause();
        if (h2 == 1) {
            pid_t hijo2_pid = fork();
            if (hijo2_pid == 0) {
                printf("Hijo2 con PID:%d, padre PID:%d\n", getpid(), padre_pid);
                char *programa = "ls";
                char *arg[] = {"ls", "-a", NULL};
                execvp(programa, arg);
                printf("Error en execvp");
            }
        }
    }

    return 0;
}

void hijos(int sig) {
    h1 = 1;
    hijos_pids[cont++] = aux;
}

void hijo2(int sig) {
    h2 = 1;
}

void TERM(int sig) {
    t++;
    h1 = 0;
    h2 = 0;
}

