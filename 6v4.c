#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>  // Necesario para waitpid

pid_t padre_pid;
pid_t hijos_pids[100];
int aux, cont = 0;
int h1 = 0, h2 = 0;
int t= 0;  // Variable para indicar si se recibió SIGTERM

void hijos(int sig);
void hijo2(int sig);
void TERM(int sig);

int main() {
    padre_pid = getpid();
    signal(SIGUSR1, hijos);
    signal(SIGUSR2, hijo2);
    signal(SIGTERM, TERM);

    printf("padre: PID=%d\n", getpid());

    h1 = 0;
    h2 = 0;
    t=0;

    while (1) {
        if (t) {
            printf("Recibida la señal SIGTERM.\n");
            break;
        }

        if (h1 != 0) {
            pid_t hijo_pid = fork();
            aux = hijo_pid;
            if (hijo_pid == 0) {
                while (1) {
                    printf("Hijo1 PID: %d, PID del padre: %d\n", getpid(), padre_pid);
                    sleep(5);
                }
                 hijos_pids[cont++] = aux;
            }
            //else{
              //  hijos_pids[cont++] = aux;
            //}
        }

        if (h2 == 1) {
            pid_t hijo2_pid = fork();
            if (hijo2_pid == 0) {
                printf("Hijo2 con PID:%d, padre PID:%d\n", getpid(), padre_pid);
                char *programa = "ls";
                char *arg[] = {"ls", "-a", NULL};
                execvp(programa, arg);
                printf("Error en execvp");
                exit(1);
            }
        }

        pause();  // Esperar a las señales
    }

    // Esperar a que todos los hijos terminen
    for (int i = 0; i < cont; i++) {
        kill(hijos_pids[i], SIGTERM);
        //waitpid(hijos_pids[i], NULL, 0);
        printf("hijo aniquilado PID: %d \n",hijos_pids[i]);
        sleep(1);
    }

    return 0;
}

void hijos(int sig) {
    h1 = 1;
}

void hijo2(int sig) {
    h2 = 1;
}

void TERM(int sig) {
    t = 1;
    h1 = 0;
    h2 = 0;
}
