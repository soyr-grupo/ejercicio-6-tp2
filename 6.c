#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

pid_t padre_pid;
pid_t hijos_pids[100];
int cont = 0;

void hijos(int sig);
void hijo2(int sig);

int main() {
    padre_pid = getpid(); // Obtener el PID del proceso padre
    signal(SIGUSR1, hijos); // Configurar el manejador de señales para SIGUSR1
    signal(SIGUSR2, hijo2); // Configurar el manejador de señales para SIGUSR2
    signal(SIGTERM, hijos); // Configurar el manejador de señales para SIGTERM

    printf("padre: PID=%d\n", getpid());

    while (1) {
        pause();
    }

    return 0;
}

void hijos(int sig) {
    if (sig == SIGUSR1) {
        pid_t hijo_pid = fork();
        if (hijo_pid == 0) {
            // Código del hijo para SIGUSR1
            while (1) {
                printf("Soy el hijo1 con PID: %d, PID del padre: %d\n", getpid(), padre_pid);
                sleep(5); // Esperar 5 segundos
            }
        } else if (hijo_pid > 0) {
            hijos_pids[cont++] = hijo_pid; // guardar hijos
        } else {
            printf("Error al crear el hijo");
            exit(1);
        }
    } else if (sig == SIGTERM) {
        printf("Recibida la señal SIGTERM. Terminando todos los hijos...\n");
        for (int i = 0; i < cont; i++) {
            printf("Hijo PID:%d\n", hijos_pids[i]);
            kill(hijos_pids[i], SIGTERM);
            sleep(1);
        }
        exit(0);
    }
}

void hijo2(int sig) {
    if (sig == SIGUSR2) {
        // Código del hijo para SIGUSR2
        printf("Hijo2 con PID:%d, padre PID:%d\n", getpid(), padre_pid);

        // Ejecutar "ls")
        char *programa = "ls";
        char *arg[] = {"ls", "-a", NULL};
        execvp(programa, arg);

        printf("Error en execvp");
        exit(1); // El hijo finaliza después de ejecutar su tarea
    }
    exit(1);
}

